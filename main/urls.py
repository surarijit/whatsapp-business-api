
from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('sms/',views.reply_to_sms_messages, name="sms"),
]